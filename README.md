# Frameworks de Front-end desde adentro

Bienvenidos al taller de frameworks de front-end desde adentro. Aquí aprenderemos la estructura y funcionamiento básico de los frameworks más utilizados hoy en día (React, Angular, Vuejs y Svelte). Crearemos nuestro propio framework desde cero con JavaScript, simple pero muy poderoso, espero que lo disfruten :relaxed:

## Primeros pasos
Antes que nada tenemos que asegurarnos que tenemos instalado NodeJS, si no lo tienes entra a https://nodejs.org/es/ y descarga la versión Recomendada para la mayoría, si manejas Linux o MacOS, la página tiene los paquetes respectivos. Para verificar que tienes instalado NodeJS basta con ejecutar el siguiente comando en la consola:
```
node -v
```
Y nos tiene que responder con la versión que tenemos instalada:
```
v12.14.1
```

## Instalar dependencias
Ya que tengamos NodeJS instalado, podemos hacer uso del manejador de paquetes <b>npm</b> que viene incluido en NodeJS.

En este repositorio existe un archivo <b>package.json</b> el cual contiene los paquetes que se van a ocupar en el proyecto. Lo único que tenemos que hacer para instalarlo es navegar a la capreta de este repositorio y ejecutar

```
npm install
```
Esperamos a que termine y con esto ya tendremos nuestro proyecto listo para empezar a trabajar, es necesario que hagan esto ya que es probable que el internet esté bastante lento, de esta manera podremos trabajar sin requerir conexión a internet durante el taller :blush:

## Recomendaciones
* El editor de código que vamos a usar es Visual Studio Code [Aquí podrán descargarlo](https://code.visualstudio.com/)
* El navegador recomendado es Google Chrome
* Si tienen imágenes o gifs que quieran usar para el proyecto sería muy útil
* [En esta página](https://www.tutorialspoint.com/es6/index.htm) hay un tutorial bastante bien explicado de JavaScript ES6, el cual vamos a usar durante el taller, los capítulos importantes son <b>Classes, RegExp, Events y Functions.</b> No es necesario dominarlas, solo como recomendación :relaxed:

![](https://media.giphy.com/media/3oeSAz6FqXCKuNFX6o/giphy.gif)