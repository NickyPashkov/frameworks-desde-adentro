import VirtualDom from './VirtualDom'

export default class Component {
  created() {}
  mounted() {}
  render() {}
  destroyed() {}

  update() {
    var currentElement = this._currentElement
    var nextElement = VirtualDom.parseVDom(this.render(), this)

    currentElement.$el = this.$el
    nextElement.$el = currentElement.$el

    VirtualDom.update(currentElement, nextElement, this)
  }
}