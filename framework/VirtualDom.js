export default {
  // Pasar de DOM Virtual a DOM REAL
  mount(el, domNode, parentComponent) {
    switch(typeof el) {
      case 'function':
        return this.mountComponent(el, domNode, parentComponent)
      case 'object':
        return this.mountElement(el, domNode, parentComponent)
      case 'string':
        return this.mountTextNode(el, domNode, parentComponent)
    }
  },
  mountComponent(el, domNode, parentComponent) {
    var component = new el()
    component.created()

    component._currentElement = this.parseVDom(component.render(), component)
    var $el = this.mount(component._currentElement, domNode, component)

    component.data = new Proxy(component.data, {
      get: (obj, prop) => {
        return obj[prop]
      },
      set: (obj, prop, value) => {
        if(obj[prop] != value) {
          obj[prop] = value
          component.update()
        }
        return true
      }
    })

    component.$el = $el
    component.mounted()

    return $el

  },
  mountElement(el, domNode, parentComponent) {
    const tag = el.elementName
    const children = el.children || []

    console.log(parentComponent)

    const $el = document.createElement(tag)
    el.$el = $el

    this.addAttributes(el, parentComponent)

    for (const child of children) {
      this.mount(child, $el, parentComponent)
    }

    domNode.appendChild($el)
    return $el
  },
  mountTextNode(el, domNode, parentComponent) {
    const $el = document.createTextNode(el)
    domNode.appendChild($el)
    return $el
  },
  parseVDom(el, component) {
    const varRegex = /%(.*?)%/g
    var data = component.data

    if(!el.children) return el

    el.children.forEach((child, i) => {
      if(typeof child === 'string') {
        var match
        while(match = varRegex.exec(child)) {
          const variable = match[1].trim()
          el.children[i] = el.children[i].replace(match[0], data[variable])
        }
      } else
        this.parseVDom(el.children[i], component)
    })

    return el
  },
  addAttributes(el, component) {
    const varRegex = /{{(.*?)}}/g

    var data = component.data
    var domNode = el.$el
    for(let key in el.attributes) {
      var match
      while(match = varRegex.exec(el.attributes[key])) {
        const variable = match[1].trim()
        el.attributes[key] = el.attributes[key].replace(match[0], data[variable])
      }
      domNode.setAttribute(key, el.attributes[key])

    }
  },
  update(currentElement, nextElement, parentComponent) {
    if(currentElement.elementName == nextElement.elementName) {
      if(nextElement.children) {
        this.updateChildren(currentElement.children, nextElement.children, currentElement.$el)
      }
    }
  },
  updateChildren(currentChildren, nextChildren, domNode) {
    for(let i = 0; i < nextChildren.length; i++) {
      var nextChild = nextChildren[i]
      var currentChild = currentChildren[i]

      if(typeof nextChild === 'string' && typeof currentChild === 'string') {
        if(currentChild !== nextChild) {
          domNode.innerHTML = nextChild
        }
      } else {
        nextChild.$el = currentChild.$el
        this.update(currentChild, nextChild)
      }
    }
  }
}