import VirtualDom from 'Framework/VirtualDom.js'

export default class Framework {
  constructor(root, $el) {
    VirtualDom.mount(root, $el)
  }
}