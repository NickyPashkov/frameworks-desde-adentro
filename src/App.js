import Component from 'Framework/Component.js'

//import 'bulma/css/bulma.min.css'
import './assets/ejemplo.styl'

export default class App extends Component {

  created() {
  }

  mounted() {
    setInterval(() => {
      this.data.numero *= 2
    }, 1000)

  }

  data = {
    numero: 2
  }

  render() {
    return (
      <div>
        <h1>Titulo</h1>
        <p>Parrafo</p>
        <p>Parrafo 2</p>
      </div>
    )
  }
}